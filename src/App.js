import React, { Component } from 'react'
import getWeb3 from './utils/getWeb3'
import { Route, Link } from 'react-router-dom'
import PropertyView from './layouts/PropertyView'
import About from './layouts/About.jsx'

import PropertyStorageProxy from '../build/contracts/PropertyStorageProxy.json'
import PropertyManagementV1 from '../build/contracts/PropertyManagementV1.json'
import PropertyConnectorV1 from '../build/contracts/PropertyConnectorV1.json'

import _ from 'underscore'

import './css/oswald.css'
import './css/open-sans.css'
import './css/pure-min.css'
import './App.css'

const weiToETH = 10**18

class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      proxyInstance: null,
      propertyConnectorInstance: null,
      properties: [],
      controllerAddress: null,
      web3: null,
      intervalId: null
    }

    this.syncState = this.syncState.bind(this)
    this.updateCost = this.updateCost.bind(this)
    this.addProperty = this.addProperty.bind(this)
  }

  async componentWillMount() {
    // Get network provider and web3 instance.
    // See utils/getWeb3 for more info.

    getWeb3
    .then(results => {
      let web3 = results.web3
      web3.eth.defaultAccount = web3.eth.accounts[0]
      this.setState({ web3 })
  

      // Instantiate contract once web3 provided.
      return this.instantiateContract()
    })
    .catch(() => {
      console.log('Error finding web3.')
    })
    
    // Incase the database has already been hydrated we will sync first
    // await this.hydrateProperites();
    await this.syncState();

  }

  componentDidMount() {
    this.syncState();
    var intervalId = setInterval(this.syncState, 1000*2);
    // store intervalId in the state so it can be accessed later:
    this.setState({intervalId: intervalId});
  }
  
  componentWillUnmount() {
    // use intervalId from the state to clear the interval
    clearInterval(this.state.intervalId);
  }

  async instantiateContract() {
    /*
     * SMART CONTRACT EXAMPLE
     *
     * Normally these functions would be called in the context of a
     * state management library, but for convenience I've placed them here.
     */

    // Create a truffle contract abstraction with the contract artifacts
    const contract = require('truffle-contract')
    const propertyStorageProxy = contract(PropertyStorageProxy)
    const propertyManagementV1 = contract(PropertyManagementV1)
    const propertyConnectorV1 = contract(PropertyConnectorV1)

    // Set the web3 provider for each of the contracts
    propertyStorageProxy.setProvider(this.state.web3.currentProvider)
    propertyManagementV1.setProvider(this.state.web3.currentProvider)
    propertyConnectorV1.setProvider(this.state.web3.currentProvider)

    // Get accounts.
    this.state.web3.eth.getAccounts((error, accounts) => {
      propertyStorageProxy.deployed()
      .then((proxyInstance) => {
        console.log('Proxy Instance:' , proxyInstance)
        // This is where the magic happens. 
        // Here we use 'underscores' extend method to leech the proxy onto 
        // the delegate 'propertyManagementV1' 
        proxyInstance = _.extend(proxyInstance, propertyManagementV1.at(proxyInstance.address))
        this.setState({proxyInstance: proxyInstance})
        // Setup the connector contract instance in state 
        return propertyConnectorV1.deployed()
      }).then((connectorInstance) => {
        console.log('Connector Instance:', connectorInstance)
        this.setState({propertyConnectorInstance: connectorInstance})
        return this.syncState()
      })
    })
  }

  async syncState() {
    const { proxyInstance, propertyConnectorInstance } = this.state
    if(proxyInstance && propertyConnectorInstance) {
      const totalProperties = await proxyInstance.getTotalPropertyCount.call();
      if(totalProperties > 0) {
        let { properties } = this.state
        // Properties are zero indexed so we can tell if they have been init'ed first
        for(var p=0; p<totalProperties; p++) {
          const id = p+1
          const name = await proxyInstance.getPropertyNameAtID(id)
          let cost = await proxyInstance.getPropertyWeiCost(name)
          cost /= weiToETH
          const property = {
            name,
            cost,
            id
          }
          properties[p] = property;
        }
        this.setState({ properties })
      } else {
        console.log("No Properties!")
      }
    } else {
      console.log("SyncState: Missing contract instances!")
    }
  }

  // TODO: need to finish the logic here 
  async updateCost(event, ethCost, id) {
    event.preventDefault()
    const { propertyConnectorInstance } = this.state
    const weiCost = Number(ethCost) * weiToETH
    try {
      await propertyConnectorInstance.setWeiCost(id, weiCost)
      await this.syncState()
    } catch(e){
      console.log(e)
    }
  }

  async addProperty(event, newPropertyName) {
    event.preventDefault()
    const { propertyConnectorInstance } = this.state
    try {
      this.setState({newPropertyName: ''})
      await propertyConnectorInstance.createDefaultProperty(newPropertyName)
    } catch(e) {
      console.log(e)
    }
  }

  render() {
    const { properties } = this.state

    const navBar = (
      <nav className="navbar pure-menu pure-menu-horizontal">
        <a href="#" className="pure-menu-heading pure-menu-link">Crypto Real Estate</a>
        <Link to='/' className="pure-menu-heading pure-menu-link">About</Link>
        <Link to='/DApp' className="pure-menu-heading pure-menu-link">DApp</Link>
    </nav>
    )
  
    return (
      <div className="App">
        
        <Route exact path="/" component={() => (
          <div>
            {navBar}
            <About />
          </div>
        )} />
        <Route path="/dapp" component={() => (
          <div>
            {navBar}
            <PropertyView
              properties={properties}
              addProperty={e => this.addProperty()}
              updateCost={(e, index) => this.updateCost(e, index)}
            />
          </div>
        )
        } />
      </div>
    );
  }
}

export default App
