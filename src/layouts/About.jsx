import React, { Component } from 'react'
import './About.css'


class About extends Component {
  
  render() {

    return (
      <main className="container">
        <h1>This DApp uses Upgradeable Smart Contracts.</h1>
        <h3> 
          Check out the repo{' '}
          <a 
            href="https://gitlab.com/BryceDoganer/UpgradeableSmartContracts/"
            target="_blank"
          > 
          here
          </a>
          {' '} for more information!
        </h3>
        <p>
          This DApp is used as an example, and as such, is very unpolished. 
        </p>
        <p>
          I do plan on adding some more functionality so submit feature requests!
        </p>
        <h3>
          To Begin: 
        </h3>
        <ol>
          <li>
            Connect Metamask to Ropsten testnet
          </li>
          <li>
            (If needed) Add testnet funds from{' '}
            <a 
              href="https://faucet.metamask.io/"
              target="_blank"
            >
            this faucet.
            </a> 
          </li>
          <li>
            Navigate to "DApp" in the nav menu to start experimenting. 
          </li>
        </ol>
      </main>
    )
  }  
}

export default About