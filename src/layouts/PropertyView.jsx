import React, { Component } from 'react'
import './PropertyView.css'

class PropertyView extends Component {
  constructor(props) {
    super(props)

    this.state = {
      newPropertyName: '',
      inputs: []
    }

    this.handleChange = this.handleChange.bind(this)
  }

  handleChange(event, index) {
    if(event.target.name === "newProperty") {
      this.setState({newPropertyName: event.target.value})
    } else if(event.target.name === "newCost") {
      const value = event.target.value
      let { inputs } = this.state
      inputs[index] = value
      this.setState({inputs})
    }
  }

  render() {  
    const { newPropertyName, inputs } = this.state
    const { properties, addProperty, updateCost } = this.props

    const propertyView = properties.map((property, index) => (
      <li key={index} className="propertyItem"> 
        <div>
          <div>
            Property Name: <span className="bold">{property.name}</span>
          </div>
          <div>
            Property Cost: <span className="bold">{property.cost} ETH</span>
          </div>
            <form onSubmit={e => updateCost(e, inputs[index], properties[index].id)}>
              <input 
                type="text" 
                name="newCost"
                value={inputs[index]}
                placeholder={"New ETH Value"}
                onChange={e => this.handleChange(e, index)}
              />
              <button>Submit New Cost</button>
            </form>
            <hr/>
        </div>
      </li>
    ))

    return (
      <div className="Property-View">
        <main className="container">
          <h1>Terrible Property Management Service</h1>
          <h3>This terrible service offers these features:</h3>
          <ol>
            <li>Browse properties and their costs.</li>
            <li>List properties as you wish.</li>
            <li>Change the value of any property as you please!</li>
          </ol>
          <h3>List a Property for sale!</h3>
          <form onSubmit={e => addProperty(e)}>
          <input 
            type="text" 
            name="newProperty"
            value={newPropertyName}
            onChange={e => this.handleChange(e)}
            placeholder="New Property Name"
          />
          <button>Submit New Property</button>
        </form>
          <h2>Property List:</h2>
          <ul>
            {propertyView}
          </ul>
        </main>
      </div>
    )
  }
}


export default PropertyView